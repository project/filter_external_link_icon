# Filter External Link Icon

This module helps with marking external links inside formatted text fields.

It provides a text filter plugin that appends a span element - <span class="external-link-mark">↗</span> - at the end of anchor tags with external links. The content of this span is configurable and can be left empty if you wish to set a CSS background image on it instead.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/filter_external_link_icon).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/filter_external_link_icon).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Visit /admin/config/content/formats
- Configure your text format
- Enable "Mark External Links"
- (Optional) Configure the content of the span appended at the end of external links


## Maintainers

- Mihai Prodan - [mihaiprodann](https://www.drupal.org/u/mihaiprodann)
- Stefan Butura - [stefan.butura](https://www.drupal.org/u/stefanbutura)
